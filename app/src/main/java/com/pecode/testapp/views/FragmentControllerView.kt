package com.pecode.testapp.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.pecode.testapp.databinding.ViewControllerBinding
import com.pecode.testapp.extensions.fadeIn
import com.pecode.testapp.extensions.fadeOutToGone
import com.pecode.testapp.extensions.setClickable
import com.pecode.testapp.extensions.setUnclickable

class FragmentControllerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val binding =
        ViewControllerBinding.inflate(LayoutInflater.from(context), this, true)

    var onPlusClick: (() -> Unit)? = null
    var onMinusClick: (() -> Unit)? = null

    var counter = DEFAULT_COUNTER
        set(value) {
            field = value
            binding.fragmentCounter.text = value.toString()
        }

    fun hideMinus() {
        binding.fragmentMinus.apply {
            setUnclickable()
            fadeOutToGone()
        }
    }

    fun showMinus() {
        binding.fragmentMinus.apply {
            setClickable()
            fadeIn()
        }
    }

    init {
        initButtons()
    }

    private fun initButtons() {
        binding.apply {
            fragmentPlus.setOnClickListener { onPlusClick?.invoke() }
            fragmentMinus.setOnClickListener { onMinusClick?.invoke() }
        }
    }

    companion object {
        private const val DEFAULT_COUNTER = 0
    }

}