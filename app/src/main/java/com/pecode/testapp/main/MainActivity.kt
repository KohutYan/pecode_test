package com.pecode.testapp.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import by.kirich1409.viewbindingdelegate.CreateMethod
import by.kirich1409.viewbindingdelegate.viewBinding
import com.pecode.testapp.databinding.ActivityMainBinding
import com.pecode.testapp.pager.PagerAdapter

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by viewBinding(CreateMethod.INFLATE)
    private val viewModel: MainViewModel by viewModels()

    private val pagerAdapter by lazy { PagerAdapter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)
        initUi()
    }

    private fun initUi() {
        initPager()
        initFragmentController()
    }

    private fun initPager() {
        createNewFragment()     //init default data

        binding.viewPager.apply {
            adapter = pagerAdapter
            initPageChangeListener(this)
        }
    }

    private fun initPageChangeListener(pager: ViewPager2) {
        pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                binding.fragmentController.counter = position + 1
            }
        })
    }

    private fun initFragmentController() {
        binding.fragmentController.apply {
            onPlusClick = { createNewFragment() }
            onMinusClick = { removeLastFragment() }
        }
    }

    private fun createNewFragment() {
        val pager = binding.viewPager
        pagerAdapter.apply {
            addFragment()
            pager.currentItem = this.itemCount
        }
        setMinusButtonState(pagerAdapter.itemCount)
    }

    private fun removeLastFragment() {
        pagerAdapter.removeLastFragment()
        setMinusButtonState(pagerAdapter.itemCount)
    }

    private fun setMinusButtonState(itemCount: Int) {
        binding.fragmentController.apply {
            if (itemCount < 2) {
                hideMinus()
            } else {
                showMinus()
            }
        }
    }

}