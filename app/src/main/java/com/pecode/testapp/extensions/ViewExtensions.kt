package com.pecode.testapp.extensions

import android.view.View

fun View.isVisible() = visibility == View.VISIBLE

fun View.isGone() = visibility == View.INVISIBLE

fun View.setVisible() { visibility = View.VISIBLE }

fun View.setGone() { visibility = View.GONE }

fun View.setClickable() { isClickable = true }

fun View.setUnclickable() { isClickable = false }


fun View.fadeOutToGone() {
    if (this.isGone().not()) {
        animate().alpha(0.0f).withEndAction {
            setGone()
        }
    }
}

fun View.fadeIn() {
    if (this.isVisible().not()) {
        alpha = 0f
        setVisible()
        animate().alpha(1.0f)
    }
}