package com.pecode.testapp.pager

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.CreateMethod
import by.kirich1409.viewbindingdelegate.viewBinding
import com.pecode.testapp.R
import com.pecode.testapp.databinding.FragmentPagerBinding
import kotlin.random.Random

class PagerFragment : Fragment() {

    private val binding: FragmentPagerBinding by viewBinding(CreateMethod.INFLATE)

    private var pageNumber: Int = DEFAULT_PAGE_NUMBER
    private val notificationIdList = mutableListOf<Int>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        pageNumber = requireArguments().getInt(resources.getString(R.string.fragment_key), 0)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()
    }

    private fun initUi() {
        createNotificationChannel()
        initCreateNotification()
    }

    private fun initCreateNotification() {
        binding.createNotification.setOnClickListener {
            showNotification()
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val descriptionText = getString(R.string.notification_channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager =
                requireContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun showNotification() {
        val notification = createNotification()

        NotificationManagerCompat.from(requireContext()).apply {
            val id = Random.nextInt()
            notificationIdList.add(id)
            notify(id, notification)
        }
    }

    private fun createNotification(): Notification {
        return NotificationCompat.Builder(requireContext(), CHANNEL_ID)
            .setContentTitle(getString(R.string.notification_title))
            .setContentText(getString(R.string.notification_text, pageNumber))
            .setSmallIcon(R.drawable.ic_message)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .build()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        deleteFragmentNotifications()
    }

    private fun deleteFragmentNotifications() {
        NotificationManagerCompat.from(requireContext()).apply {
            notificationIdList.forEach {
                cancel(it)
            }
        }
    }

    companion object {
        private const val CHANNEL_ID = "channel_id"
        private const val CHANNEL_NAME = "channel_name"
        private const val DEFAULT_PAGE_NUMBER = 0
    }

}