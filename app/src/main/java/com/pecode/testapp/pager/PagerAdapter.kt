package com.pecode.testapp.pager

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.pecode.testapp.R

class PagerAdapter(private val fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    private val list: MutableList<Int> = mutableListOf()

    fun addFragment() {
        list.add(this.itemCount + 1)
        notifyItemInserted(this.itemCount)
    }

    fun removeLastFragment() {
        list.removeLast()
        notifyItemRemoved(this.itemCount)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun createFragment(position: Int): Fragment = PagerFragment().apply {
        arguments = bundleOf(
            fragmentActivity.getString(R.string.fragment_key) to list[position]
        )
    }
}